const csvWriter = require('csv-writer').createObjectCsvWriter({
  path: 'Test3.csv',
  header: [
    {id: 'name', title: 'Speaker'},
    {id: 'topic', title: 'Topic'},
    {id: 'date', title: 'Date'},
    {id: 'words', title: 'Words'},
  ]
});

const names = [
  'Alexander Abel',
  'Bernhard Belling',
  'Caesare Collins',
  'Max Muster',
  'Tom Tester'
]

const topics = [
  'Education Policy',
  'Coal Subsidies',
  'Inner Security'
]

const data = []
for (let i = 0; i < 50000; i++) {
  data.push({
    name: names[Math.trunc(Math.random() * names.length)],
    topic: topics[Math.trunc(Math.random() * topics.length)],
    date: `${Math.trunc(Math.random() * 10) + 2005}-12-11`,
    words: Math.trunc(Math.random() * 10000)
  })
}

csvWriter
  .writeRecords(data)
  .then(()=> console.log('The CSV file was written successfully'));
