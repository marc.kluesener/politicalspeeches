# PoliticalSpeeches

### Run this app 

#### With Node and NPM
Tested with node Versions 8, 12, 14 and 16 \
`npm install` \
`npm run dev`

Then open http://localhost:8082/evaluation?url=https://storage.googleapis.com/political-speeches-evaluation-test-data/Beispiel.csv

- - -

#### With Docker 
`docker build . -t political-speeches` \
`docker run -p <YOUR_LOCAL_PORT>:8080 political-speeches`

Please note: `localhost` will refer to the docker instance, so you will have to use your local IP address, if you want to reference a file on a local server.

- - -

#### Hosted 
You can also use the deployed version of the app: https://political-speech-evaluation-lkhp74wy7a-ey.a.run.app/evaluation?url=https://storage.googleapis.com/political-speeches-evaluation-test-data/Beispiel.csv


### Testdata

You can use these test files: \
https://storage.googleapis.com/political-speeches-evaluation-test-data/Beispiel.csv \
https://storage.googleapis.com/political-speeches-evaluation-test-data/Beispiel2.csv \
https://storage.googleapis.com/political-speeches-evaluation-test-data/50kZeilen.csv




