require('dotenv').config()
const express = require('express')
const app = express()

require('./evaluation/EvaluationController')(app)

const port = process.env.PORT
app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`)
})
