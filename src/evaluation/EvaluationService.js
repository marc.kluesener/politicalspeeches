const fetch = require('node-fetch')
const csv = require('fast-csv')

const RELEVANT_YEAR = 2013
const RELEVANT_TOPIC = 'Inner Security'

function checkHighestValue (currentHighest, valuesPerSpeaker, speaker) {
  if (valuesPerSpeaker[speaker] === currentHighest.value) {
    currentHighest.speaker = null
  } else if (valuesPerSpeaker[speaker] > currentHighest.value) {
    currentHighest.value = valuesPerSpeaker[speaker]
    currentHighest.speaker = speaker
  }
}

function checkLowestValue (currentLowest, valuesPerSpeaker, speaker) {
  if (valuesPerSpeaker[speaker] === currentLowest.value) {
    currentLowest.speaker = null
  } else if (valuesPerSpeaker[speaker] < currentLowest.value) {
    currentLowest.value = valuesPerSpeaker[speaker]
    currentLowest.speaker = speaker
  }
}

async function loadAndEvaluateUrl (urls) {
  const wordsPerSpeaker = {}
  const speeches2013PerSpeaker = {}
  const speechesInnerSecurityPerSpeaker = {}

  for (const url of urls) {
    // fast-csv streaming API is using callbacks, wrapping in a Promise to be able to 'await' it
    await new Promise(async (resolve, reject) => {
      let response
      try {
        response = await fetch(url)
      } catch (e) {
        reject('Cannot load ' + url)
      }

      if (!response.ok) {
        reject('Cannot load ' + url)
      }
      response.body
        .pipe(csv.parse({
          headers: ['Speaker', 'Topic', 'Date', 'Words'],
          trim: true
        }))
        .on('error', error => reject(error))
        .on('data', row => {
          const speaker = row.Speaker

          wordsPerSpeaker[speaker] = (wordsPerSpeaker[speaker] || 0) + Number.parseInt(row.Words)

          if (row.Topic === RELEVANT_TOPIC) {
            speechesInnerSecurityPerSpeaker[speaker] = (speechesInnerSecurityPerSpeaker[speaker] || 0) + 1
          }

          const date = new Date(row.Date)
          if (date.getFullYear() === RELEVANT_YEAR) {
            speeches2013PerSpeaker[speaker] = (speeches2013PerSpeaker[speaker] || 0) + 1
          }
        })
        .on('end', (rowCount) => {
          console.log(`Parsed ${rowCount} rows for ${url}`)
          resolve(true)
        })
    })
  }

  const currentLeastWords = {speaker: null, value: Number.MAX_SAFE_INTEGER}
  const currentMaxInnerSecurity = {speaker: null, value: 0}
  const currentMaxSpeechesIn2013 = {speaker: null, value: 0}
  Object.keys(wordsPerSpeaker).forEach(speaker => checkLowestValue(currentLeastWords, wordsPerSpeaker, speaker))
  Object.keys(speechesInnerSecurityPerSpeaker).forEach(speaker => checkHighestValue(currentMaxInnerSecurity, speechesInnerSecurityPerSpeaker, speaker))
  Object.keys(speeches2013PerSpeaker).forEach(speaker => checkHighestValue(currentMaxSpeechesIn2013, speeches2013PerSpeaker, speaker))

  return {
    leastWordy: currentLeastWords.speaker,
    mostSecurity: currentMaxInnerSecurity.speaker,
    mostSpeeches: currentMaxSpeechesIn2013.speaker
  }
}

module.exports = {
  loadAndEvaluateUrl
}
