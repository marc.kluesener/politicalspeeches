const { loadAndEvaluateUrl } = require('./EvaluationService')

module.exports = function (app) {
  app.get('/evaluation', async (req, res) => {
    if (!req.query.url) {
      res.status(400).send('Error: param "url" is missing')
      return
    }

    let urls
    if (typeof req.query.url === 'string') {
      urls = [req.query.url]
    } else {
      urls = req.query.url
    }

    res.send(await loadAndEvaluateUrl(urls))
  })
}
